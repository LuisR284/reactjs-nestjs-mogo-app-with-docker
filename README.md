# README
React - NestJS - Mongo app example using docker

## Structure
```bash
apps/ # Where your differents apps goes (client and server) each app has his own dockerfile
  client/ # Made with React
    dockerfile.client
    .env # If you are running locally make sure to set the righ server port
  server/ # Made with NestJS
    dockerfile.server
infra/ # here you place all the scripts needed
  jobs/ # scripts for automatization process
    crontab.txt
    dockerfile
    entry.sh
  mongo-init.js # Initial mongo script, it creates the mongodb user
  nginx.conf # Custom nginx config
database/ #mongodb files
  .gitignore #you can delete this file if you want to upload all those files
.env # Config file, here you set apps port
.eslintignore
.eslintrc.js
.gitignore
.prettierignore
.prettierrc.json
package.json # Monorepo package
README.md # This file!
```

## Local Run
```bash
$ yarn install
$ yarn setup
$ docker-compose up mongo
$ yarn start:server:dev
$ yarn start:client .env
```

## Dockerized Run
```bash
$ docker-compose up
```

## Notes
* At the moment of writing this the node lts is 14.16.0
* In local enviroment there can be an issue using workspaces with yarn 1.2.* try using the version is 1.19.1 or use yarn2
* The credentials setted on `infra/mongo-init.js` must be the same that the ones on `app/server/src/app.module.ts`
* Tested with the currrent stable release of docker-compose [Click here for more info](https://docs.docker.com/compose/install/)
```bash
$ docker-compose --version
$ docker-compose version 1.28.6, build 5db8d86f
```
* If you have any errors and dont want to upgrade docker-compose try changing the version in the `docker-compose.yml` to 3.5 or above
* There is a cronjob executing every hour that refreshes the db, however you can do it manually with this endpoint `http://localhost:${SERVER_PORT}/job/notices`
* In the docker.compose.yml there is a service called reign-job, if you want to use this instead of NestJS HandleCron, all you have to do if remove `TasksService` from providers in in `apps/server/src/app.module.ts` and uncomment the reign-job service
* Default ports:
```bash
$ CLIENT_PORT=8080
$ SERVER_PORT=3000
$ MONGO_PORT=27017
```
* Page size hardcoded to 50

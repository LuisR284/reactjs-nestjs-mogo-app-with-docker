export interface ParsedNews {
  _id: string
  title?: string
  url?: string
  story_url?: string
  author: string
  story_id?: number
  story_title?: string
  created_at: string
}

export interface Notices {
  notices: ParsedNews[]
  totalNumberOfNotices: number
}

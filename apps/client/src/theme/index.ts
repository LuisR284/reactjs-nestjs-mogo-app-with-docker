import { Theme } from '@emotion/react'

export const theme: Theme = {
  colors: {
    dark: '#2E2E2E',
    grey: '#fafafa',
    text: '#333',
    border: '#ccc',
    lightText: '#999',
    white: '#fff',
    disabled: '#ddd',
  },
}

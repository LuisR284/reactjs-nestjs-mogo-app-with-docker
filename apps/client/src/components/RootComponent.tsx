import { ThemeProvider } from '@emotion/react'
import React from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import { HomePage } from 'views/Home'
import { hist } from '../lib/utils/history'
import { theme } from 'theme'

export const RootComponent: React.FC<any> = ({ children, props }) => {
  return (
    <ThemeProvider theme={theme}>
      <Router history={hist}>
        <Switch>
          <Route path="/home" component={HomePage} />
          <Redirect from="*" to="/home" />
        </Switch>
      </Router>
    </ThemeProvider>
  )
}

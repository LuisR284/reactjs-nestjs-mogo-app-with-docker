import React from 'react'
import styled from '@emotion/styled'
import { theme } from 'theme'

const BannerContainer = styled.div({
  backgroundColor: theme.colors.dark,
  color: theme.colors.white,
  padding: 30,
  paddingBottom: 45,
})

const Title = styled.p({
  fontSize: '4rem',
  fontWeight: 'bold',
  margin: 0,
})

const SubTitle = styled.p({
  fontWeight: 'bold',
  margin: 0,
})

export const Banner = () => {
  return (
    <BannerContainer>
      <Title>HN FEED</Title>
      <SubTitle>We {'<3'} hacker news!</SubTitle>
    </BannerContainer>
  )
}

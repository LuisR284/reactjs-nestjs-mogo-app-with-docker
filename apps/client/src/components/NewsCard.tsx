import React from 'react'
import styled from '@emotion/styled'
import { theme } from 'theme'
import { ReactComponent as TrashCan } from '../assets/trash-2.svg'
import { isToday, isYesterday, format } from 'date-fns'

const NewsCardContainer = styled.div({
  display: 'flex',
  borderBottom: '1px solid',
  borderColor: theme.colors.border,
  transition: 'all ease-in-out .4s',
  ':hover': {
    backgroundColor: theme.colors.grey,
  },
})

const DarkText = styled.p({
  color: theme.colors.text,
})

const Info = styled(DarkText)({
  flex: 1,
})

const DateLabel = styled(DarkText)({
  margin: 'auto',
  marginLeft: '1rem',
  marginRight: '1rem',
})

const LightText = styled.span({
  color: theme.colors.lightText,
})

const Icon = styled.div({
  margin: 'auto',
  marginInline: '2rem',
  svg: {
    cursor: 'pointer',
  },
})

const InfoContainer = styled.div({
  display: 'flex',
  flex: 1,
  cursor: 'pointer',
})

interface NewsCardProps {
  id: string
  title: string
  author: string
  date: string
  url?: string
  removeCallback: (id: string) => Promise<void>
}

export const NewsCard: React.FC<NewsCardProps> = ({
  id,
  title,
  author,
  date,
  url = '',
  removeCallback,
}) => {
  const formatDate = (dateString: string) => {
    const date = new Date(dateString)
    return isToday(date)
      ? format(date, 'h:mm aaa')
      : isYesterday(date)
      ? 'Yesterday'
      : format(date, 'MMM d')
  }

  const openInNewTab = (url: string) => {
    console.log(url)
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }

  return (
    <NewsCardContainer>
      <InfoContainer onClick={() => openInNewTab(url)}>
        <Info>
          {`${title} -`} <LightText> {author} </LightText>
        </Info>
        <DateLabel>{formatDate(date)} </DateLabel>
      </InfoContainer>
      <Icon>
        <TrashCan onClick={() => removeCallback(id)} />
      </Icon>
    </NewsCardContainer>
  )
}

import React from 'react'
import styled from '@emotion/styled'
import { theme } from 'theme'

interface PaginatorProps {
  pages: number
  currentPage: number
  onPageChange: (page: number) => void
}

const Container = styled.div({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignContent: 'center',
  marginBottom: 10,
  paddingTop: 10,
})

const DarkButton = styled.button({
  backgroundColor: theme.colors.dark,
  color: theme.colors.white,
  border: 'none',
  padding: 10,
  paddingInline: 25,
  marginInline: 20,
  width: 'max-content',
  cursor: 'pointer',
  fontWeight: 'bold',
  ':disabled': {
    backgroundColor: theme.colors.lightText,
    cursor: 'not-allowed',
  },
})

const Pages = styled.p({ margin: 0, marginBlock: 'auto' })

export const Paginator: React.FC<PaginatorProps> = ({
  pages,
  currentPage,
  onPageChange,
}) => {
  return (
    <Container>
      <DarkButton
        disabled={currentPage === 0}
        onClick={() => onPageChange(currentPage - 1)}
      >
        Previous
      </DarkButton>
      <Pages>{`Page ${currentPage + 1} of ${pages === 0 ? 1 : pages}`}</Pages>
      <DarkButton
        disabled={currentPage + 1 === pages || pages === 0}
        onClick={() => onPageChange(currentPage + 1)}
      >
        Next
      </DarkButton>
    </Container>
  )
}

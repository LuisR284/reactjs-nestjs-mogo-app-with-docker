import '@emotion/react'

declare module '@emotion/react' {
  export interface Theme {
    colors: {
      dark: string
      grey: string
      text: string
      border: string
      lightText: string
      white: string
      disabled: string
    }
  }
}

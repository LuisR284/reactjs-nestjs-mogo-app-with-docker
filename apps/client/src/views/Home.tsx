import React, { useEffect, useState } from 'react'
import { Banner } from 'components/Banner'
import styled from '@emotion/styled'
import { NewsCard } from 'components/NewsCard'
import { Paginator } from 'components/Paginator'
import { Notices, ParsedNews } from 'lib/types/ParsedNews'

const SERVER_PORT = process.env.REACT_APP_SERVER_PORT

const Container = styled.div({
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
})

const NewsContainer = styled.div({
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  padding: 10,
  paddingRight: 20,
  paddingLeft: 20,
  overflowY: 'scroll',
})

const NoData = styled.h1({
  fontWeight: 'bold',
  margin: 'auto',
  textAlign: 'center',
})

export const HomePage = () => {
  const [pages, setPages] = useState(0)
  const [currentPage, setCurrentPage] = useState(0)
  const [data, setData] = useState<ParsedNews[]>([])

  const getPage = async (page: number) => {
    const response = await fetch(
      `http://localhost:${SERVER_PORT}/notices?page=${page}`,
    )
    const notices: Notices = await response.json()
    setData(notices.notices)
    setPages(Math.ceil(notices.totalNumberOfNotices / 50))
  }

  const changePage = (page: number) => {
    setCurrentPage(page)
  }

  const deleteNotice = async (id: string) => {
    await fetch(`http://localhost:${SERVER_PORT}/notices?id=${id}`, {
      method: 'delete',
    })
    getPage(currentPage)
  }

  const News = () => (
    <>
      {data.length ? (
        data.map(
          (
            { _id, title, story_title, author, created_at, url, story_url },
            index,
          ) => (
            <NewsCard
              key={index}
              id={_id}
              author={author}
              url={url ?? story_url}
              title={title ?? story_title ?? ''}
              date={created_at}
              removeCallback={deleteNotice}
            />
          ),
        )
      ) : (
        <NoData> There is no data yet, try again later </NoData>
      )}
    </>
  )

  useEffect(() => {
    const getData = async () => {
      getPage(currentPage)
    }
    getData()
  }, [])

  return (
    <Container>
      <Banner />
      <NewsContainer>
        <News />
      </NewsContainer>
      <Paginator
        pages={pages}
        currentPage={currentPage}
        onPageChange={changePage}
      />
    </Container>
  )
}

export interface ParsedNotice {
  title?: string
  url?: string
  story_url?: string
  author?: string
  story_id?: number
  story_title?: string
  created_at?: string
}

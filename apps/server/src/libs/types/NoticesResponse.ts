import { NoticeDocument } from 'src/schema/notices.schema'

export interface Notices {
  notices: NoticeDocument[]
  totalNumberOfNotices: number
}

export interface ApiResponse {
  hits: Hit[]
  nbHits: number
  page: number
  nbPages: number
  hitsPerPage: number
  exhaustiveNbHits: boolean
  query: string
  params: string
  processingTimeMS: number
}

export interface Hit {
  created_at: string
  title?: string
  url?: string
  author: string
  points?: number
  story_text?: any
  comment_text?: string
  num_comments?: number
  story_id?: number
  story_title?: string
  story_url?: string
  parent_id?: number
  created_at_i: number
  _tags: string[]
  objectID: string
  _highlightResult: HighlightResult
}

interface HighlightResult {
  author: Author
  comment_text?: Author
  story_title?: Storytitle
  story_url?: Storytitle
  title?: Storytitle
  url?: Url
}

interface Url {
  value: string
  matchLevel: string
  fullyHighlighted: boolean
  matchedWords: string[]
}

interface Storytitle {
  value: string
  matchLevel: string
  matchedWords: any[]
}

interface Author {
  value: string
  matchLevel: string
  matchedWords: string[]
  fullyHighlighted?: boolean
}

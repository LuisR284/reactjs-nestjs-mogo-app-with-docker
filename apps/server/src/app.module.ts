import { HttpModule, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ScheduleModule } from '@nestjs/schedule'

import { NoticeModule } from './notice/notice.module'

const DB_URL: string = process.env.DB_URL ?? 'mongodb://localhost/reign'

@Module({
  imports: [
    MongooseModule.forRoot(DB_URL, {
      user: 'reign',
      pass: 'reign',
      useCreateIndex: true,
    }),
    ScheduleModule.forRoot(),
    NoticeModule,
    HttpModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}

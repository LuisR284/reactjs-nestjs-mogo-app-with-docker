import { HttpService, Injectable, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Cron, CronExpression } from '@nestjs/schedule'
import { AxiosResponse } from 'axios'

import { Model } from 'mongoose'
import { Observable } from 'rxjs'
import { first, map } from 'rxjs/operators'
import { Hit } from 'src/libs/types/ApiResponse'
import { Notices } from 'src/libs/types/NoticesResponse'
import { ParsedNotice } from 'src/libs/types/ParsedNotice'
import { Notice, NoticeDocument } from 'src/schema/notices.schema'

const NEWS_ENDPOINT =
  'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'

@Injectable()
export class NoticeService {
  private readonly logger = new Logger(NoticeService.name)

  constructor(
    private httpService: HttpService,
    @InjectModel(Notice.name) private noticeModel: Model<NoticeDocument>,
  ) {}

  async delete(id: string): Promise<number> {
    const res = await this.noticeModel
      .updateOne({ _id: id }, { deleted: true })
      .exec()
    return res.nModified
  }

  async create(notice: ParsedNotice): Promise<Notice> {
    const createdNotice = new this.noticeModel(notice)
    return createdNotice.save()
  }

  async findByPage(page = 0): Promise<Notices> {
    const totalNumberOfNotices = await this.noticeModel.countDocuments().exec()
    const notices = await this.noticeModel
      .find({
        deleted: false,
      })
      .sort({ created_at: -1 })
      .skip(50 * page)
      .limit(50)

      .exec()
    return {
      notices,
      totalNumberOfNotices,
    }
  }

  getNotices(): Observable<ParsedNotice[]> {
    return this.httpService.get(NEWS_ENDPOINT).pipe(
      map((axiosResponse: AxiosResponse) => {
        const response: Hit[] = axiosResponse.data.hits.filter(
          (hit: Hit) =>
            (hit.story_title || hit.title) &&
            (hit.url || hit.story_url) &&
            hit.story_id,
        )
        const parsedNotice: ParsedNotice[] = response.map(el => ({
          title: el.title,
          url: el.url,
          story_url: el.story_url,
          author: el.author,
          story_id: el.story_id,
          story_title: el.story_title,
          created_at: el.created_at,
        }))
        return parsedNotice
      }),
    )
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleCron() {
    this.logger.debug('Cron job started: Updating DB')
    const response = await this.getNotices().pipe(first()).toPromise()
    response.forEach(notice => this.create(notice))
    this.logger.debug('Cron job end')
  }
}

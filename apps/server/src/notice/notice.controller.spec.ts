import { HttpModule, INestApplication } from '@nestjs/common'
import { getModelToken } from '@nestjs/mongoose'
import { Test, TestingModule } from '@nestjs/testing'
import { Notice } from 'src/schema/notices.schema'
import { NoticeController } from './notice.controller'
import { NoticeService } from './notice.service'
import * as request from 'supertest'

const mockNotice = {
  _id: '6067cc22ffa33f4b3ac97a84',
  author: 'Flockster',
  created_at: '2021-04-02T20:12:15.000Z',
  story_id: 26673221,
  story_title: 'Show HN: Grit – a multitree-based personal task manager',
  story_url: 'https://github.com/climech/grit',
  title: null,
  url: null,
}

describe('NoticeController', () => {
  let controller: NoticeController
  let app: INestApplication

  beforeEach(async () => {
    class mockModel {
      savedNotice = [mockNotice]
      data
      mockModel(dto) {
        this.data = dto
      }

      save = dto => {
        return this.data
      }

      countDocuments() {
        return { exec: () => this.savedNotice.length }
      }

      find() {
        return {
          sort: () => ({
            skip: () => ({ limit: () => ({ exec: () => this.savedNotice }) }),
          }),
        }
      }

      updateOne() {
        return { exec: () => 1 }
      }
    }

    const module: TestingModule = await Test.createTestingModule({
      controllers: [NoticeController],
      providers: [
        NoticeService,
        { provide: getModelToken(Notice.name), useClass: mockModel },
      ],
      imports: [HttpModule],
    }).compile()

    controller = module.get<NoticeController>(NoticeController)
    app = module.createNestApplication()
    await app.init()
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it(`CronJob`, () => {
    return request(app.getHttpServer()).get('/job/notices').expect(200).expect({
      message: 'DB update successfully',
    })
  })

  it(`get notices`, () => {
    return request(app.getHttpServer())
      .get('/notices')
      .expect(200)
      .expect({
        notices: [mockNotice],
        totalNumberOfNotices: 1,
      })
  })

  it(`delete notice`, () => {
    return request(app.getHttpServer())
      .delete('/notices?id=60687aeaa5f5e258e65bc59d')
      .expect(200)
  })
})

import { HttpModule, Module } from '@nestjs/common'
import { NoticeService } from './notice.service'
import { NoticeController } from './notice.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { Notice, NoticeSchema } from 'src/schema/notices.schema'

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: Notice.name, schema: NoticeSchema }]),
  ],
  providers: [NoticeService],
  controllers: [NoticeController],
})
export class NoticeModule {}

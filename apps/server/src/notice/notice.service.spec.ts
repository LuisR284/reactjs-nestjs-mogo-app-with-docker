/* eslint-disable @typescript-eslint/no-empty-function */
import { HttpModule } from '@nestjs/common'
import { getModelToken } from '@nestjs/mongoose'
import { Test, TestingModule } from '@nestjs/testing'
import { Notice } from 'src/schema/notices.schema'
import { NoticeService } from './notice.service'

describe('NoticeService', () => {
  let service: NoticeService
  class mockModel {
    constructor() {}
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NoticeService,
        {
          provide: getModelToken(Notice.name),
          useClass: mockModel,
        },
      ],
      imports: [HttpModule],
    }).compile()
    service = module.get<NoticeService>(NoticeService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })
})

import { Controller, Delete, Get, HttpStatus, Query, Res } from '@nestjs/common'
import { first } from 'rxjs/operators'
import { Response } from 'express'
import { NoticeService } from './notice.service'

@Controller()
export class NoticeController {
  constructor(private readonly appService: NoticeService) {}

  @Get('/job/notices')
  async updateNotices(@Res() res: Response) {
    try {
      const response = await this.appService
        .getNotices()
        .pipe(first())
        .toPromise()
      response.forEach(notice =>
        this.appService
          .create(notice)
          .catch(error => console.error('Duplicated notice')),
      )
      return res
        .status(HttpStatus.OK)
        .json({ message: 'DB update successfully' })
    } catch (error) {
      console.error(error)
      return res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'Internal Server error' })
    }
  }

  @Get('/notices')
  async findByPage(@Res() res: Response, @Query('page') page: number) {
    try {
      const response = await this.appService.findByPage(page)
      return res.status(HttpStatus.OK).json(response)
    } catch (error) {
      console.error(error)
      return res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'Internal Server error' })
    }
  }

  @Delete('/notices')
  async deleteNotices(@Res() res: Response, @Query('id') id: string) {
    try {
      const response = await this.appService.delete(id)
      return res.status(HttpStatus.OK).json({ deleted: response })
    } catch (error) {
      console.error(error)
      return res
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .json({ message: 'Internal Server error' })
    }
  }
}

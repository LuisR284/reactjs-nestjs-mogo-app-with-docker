import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, model } from 'mongoose'

export type NoticeDocument = Notice & Document

@Schema({})
export class Notice {
  @Prop()
  title?: string
  @Prop()
  url?: string
  @Prop()
  story_url?: string
  @Prop()
  author?: string
  @Prop({ index: true, unique: true })
  story_id?: number
  @Prop()
  story_title?: string
  @Prop()
  created_at?: Date
  @Prop({ default: false })
  deleted?: boolean
}

export const NoticeSchema = SchemaFactory.createForClass(Notice)
export const NoticeModel = model('notice', NoticeSchema)
